# Code from Yunus UNUSED
def skewSymMat(S):
    skew = np.array([[    0, -S[2],  S[1]],
                     [ S[2],     0, -S[0]],
                     [-S[1],  S[0],    0]])
    return skew


# Code from Yunus UNUSED
def projectionsFromFundamental(F):
    '''
    P1 = [I|0]
    P2 = [SF|e']  # Result 9.13 in MVG book page 256
    '''

    P1 = np.array([[1,0,0,0],
                   [0,1,0,0],
                   [0,0,1,0]])


    P2 = np.zeros((3,4))
    
    u,s,vh = np.linalg.svd(F.T) # (e'.T)F = 0 >> (F.T)e' = 0, solve for e'
    e_prime = vh[-1]
    
    S = skewSymMat(e_prime)
    
    P2[:3,:3] = np.dot(S,F)
    P2[:3, 3] = e_prime
    
    
    verifyLeft = np.dot(np.dot(P2[:3].T,F),P1[:3])[:3,:3] # ([SF|e'].T)F[I|0]
    verifyRight  = np.dot(np.dot(F.T,S.T),F)  # (F.T)(S.T)F
    
    assert np.alltrue(verifyLeft == verifyRight) # Eqn. 9.9 in MVG book page 256
    
    return P1, P2

# From Yunus Proj 3 Projection Estimation Code UNUSED
def list_projections(data_list,img_list_l):
    P1s = []
    P2s = []

    for sets in data_list:
        P1,P2 = projectionsFromFundamental(sets.f_mat)
        P1s.append(P1)
        P2s.append(P2)
        
        if VERBOSE:
            print("P1: ")
            print(P1)
            print("P2: ")
            print(P2)
            print("\n")
        
    '''
    x = P1.X
    x = P2.X
    '''

    for i in range(0,img_list_l-1):
        xi = data_list[i].srcPts[0,0] # x pixel coordinates for first feature in img 1 
        yi = data_list[i].srcPts[0,1] # y pixel coordinates for first feature in img 1
        xj = data_list[i].dstPts[0,0] # x pixel coordinates for first feature in img 2
        yj = data_list[i].dstPts[0,1] # y pixel coordinates for first feature in img 2
        A = np.array([np.dot(xi, P1s[i][2,:]) - P1s[i][0,:],
                      np.dot(yi, P1s[i][2,:]) - P1s[i][1,:],
                      np.dot(xj, P2s[i][2,:]) - P2s[i][0,:],
                      np.dot(yj, P2s[i][2,:]) - P2s[i][1,:]]) # MVG book page 312
        u,s,vh = np.linalg.svd(A) # AX = 0, solve for X
        X = vh[-1]
        X = X/X[-1] # normalize to [X,Y,Z,1]
    
        xyi = np.dot(P1s[i],X) # x  = P1.X
        xyi/=xyi[-1]
        xyj = np.dot(P2s[i],X) # x' = P2.X
        xyj/=xyj[-1]
    
        #xi ~= x[0], yi ~= x[1], xj ~= x'[0], yj ~= x'[1]
        print(xi,xyi[0],yi,xyi[1],xj,xyj[0],yj,xyj[1],'\n')
  
