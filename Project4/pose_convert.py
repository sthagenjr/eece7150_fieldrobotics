#!/usr/bin/env python
import rospy
import tf
from nav_msgs.msg import Path
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Quaternion

# Set Topic
TOPIC_VAL = 0

if TOPIC_VAL == 0:
    topic = "/vehicle/path"
else:
    topic = 'imu/imu'

i = 0
    
def callback(data):

    if TOPIC_VAL == 0:
        timestamp = data.header.stamp
        i = data.header.seq

        print(len(data.poses))
        print('\n')
        x = data.poses[-1].pose.position.x
        y = data.poses[-1].pose.position.y
        z = data.poses[-1].pose.position.z
        qx = data.poses[-1].pose.orientation.x
        qy = data.poses[-1].pose.orientation.y
        qz = data.poses[-1].pose.orientation.z
        qw = data.poses[-1].pose.orientation.w        

        tum_list = [timestamp, x, y, z, qx, qy, qz, qw]
        
        with open('imu_data.tum','a') as f:

            f.write(str(timestamp))
            f.write(' ')
            f.write(str(x))
            f.write(' ')
            f.write(str(y))
            f.write(' ')
            f.write(str(z))
            f.write(' ')
            f.write(str(qx))
            f.write(' ')
            f.write(str(qy))
            f.write(' ')
            f.write(str(qz))
            f.write(' ')
            f.write(str(qw))
            f.write('\n')

        #i+=1

    #print(data)
    
    
def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('converter', anonymous=True)

    rospy.Subscriber(topic, Path, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()
