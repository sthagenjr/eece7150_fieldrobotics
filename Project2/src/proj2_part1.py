import cv2
import sys
import numpy as np
import matplotlib.pyplot as plt

DEBUG = True
alpha = 0.5

#Function to calculate Project Matrix P coupled with t
def calc_p_mat(numPts, srcPts, dstPts):

    # Construct Empty List
    p_list = [] # Would have used np.empty but it was constructing with additional index...

    # Loop through selected Points
    for pt1, pt2 in zip(srcPts, dstPts):
        
        x = int(pt1[0])
        y = int(pt1[1])
        x_p = int(pt2[0])
        y_p = int(pt2[1])

        # From Lecture 2, hw 2.pdf Calculating homography
        tempArray = np.array([[x,y,1,0,0,0,-x*x_p,-y*x_p,-x_p],[0,0,0,x,y,1,-x*y_p,-y*y_p,-y_p]])
        tempArray.shape = (2,9)
        p_list.append(tempArray)

        if DEBUG == True:
            print('\n\n')
            print(p_list)

    p_mat = np.array(p_list)
    p_mat.shape = (2*numPts,9)

    return p_mat

#Main Function for AFR Project 1 Part A
def main(numPts):

    # Load Image 1 and Select Points
    img_center = cv2.imread('images/old_nemesis/img2.jpg', cv2.IMREAD_COLOR)
    img_centerL = cv2.imread('images/old_nemesis/img1.jpg',cv2.IMREAD_COLOR)
    img_centerR = cv2.imread('images/old_nemesis/img3.jpg',cv2.IMREAD_COLOR)
    img_left = cv2.imread('images/davisImages/PXL_20210129_2.jpg',cv2.IMREAD_COLOR)
    img_right = cv2.imread('images/davisImages/PXL_20210129_6.jpg',cv2.IMREAD_COLOR)
    img_farL = cv2.imread('images/davisImages/PXL_20210129_1.jpg',cv2.IMREAD_COLOR)
    img_farR = cv2.imread('images/davisImages/PXL_20210129_7.jpg',cv2.IMREAD_COLOR)

    #img_list_ordered = [img_left,img_centerL,img_center,img_centerR,img_right]
    #img_list = [img_center,img_centerL,img_centerR,img_left,img_right]

    img_list_ordered = [img_centerL,img_center,img_centerR]

    #name_list = ['img_left','img_centerL','img_center','img_centerR','img_right']
    name_list = ['img_centerL','img_center','img_centerR']
    
    #point_dict = {}
    
    fig1 = plt.figure()
    imgPlt = plt.imshow(img_centerL)
    srcPts = np.array(plt.ginput(n=numPts,timeout=90,show_clicks=True))

    fig2 = plt.figure()
    imgPlt = plt.imshow(img_center)
    dstPts = np.array(plt.ginput(n=numPts,timeout=90,show_clicks=True))
    plt.clf()
    
    plt.close('all')

    # Calculate P Matrix & H Matrix for centerLeft img to center img
    p_mat_c2cL = calc_p_mat(numPts, srcPts, dstPts)
 
    h_mat_c2cL,res,rank,s = np.linalg.lstsq(p_mat_c2cL[:,:-1],-p_mat_c2cL[:,-1],rcond=-1)
    h_mat_c2cL = -1*np.append(h_mat_c2cL,1)
    h_mat_c2cL.shape = (3,3)

    print(h_mat_c2cL)

    
    if DEBUG:
        h_mat,m = cv2.findHomography(srcPts,dstPts)

    # Calculate P Matrix & H Matrix for Left img to centerLeft img
    #srcPts = point_dict[name_list[0]]
    #dstPts = point_dict[name_list[1]]
    #p_mat_cL2L = calc_p_mat(numPts, srcPts, dstPts)
 
    #h_mat_cL2L = np.dot(np.linalg.inv(p_mat_cL2L[:,:-1]),-p_mat_cL2L[:,-1])
    #h_mat_cL2L = -1*np.append(h_mat_cL2L,1)
    #h_mat_cL2L.shape = (3,3)

    #print(h_mat_cL2L)

        
    fig3 = plt.figure()
    imgPlt = plt.imshow(img_centerR)
    srcPts = np.array(plt.ginput(n=numPts,timeout=90,show_clicks=True))
    plt.clf()

    fig4 = plt.figure()
    imgPlt = plt.imshow(img_center)
    dstPts = np.array(plt.ginput(n=numPts,timeout=90,show_clicks=True))
    plt.clf()

    plt.close('all')
    
    # Calculate P Matrix & H Matrix for centerRight img to center img
    p_mat_c2cR = calc_p_mat(numPts, srcPts, dstPts)
 
    h_mat_c2cR,res,rank,s = np.linalg.lstsq(p_mat_c2cR[:,:-1],-p_mat_c2cR[:,-1],rcond=-1)
    h_mat_c2cR = -1*np.append(h_mat_c2cR,1)
    h_mat_c2cR.shape = (3,3)

    #print(h_mat_c2cR)

    # Calculate P Matrix & H Matrix for Right img to centerRight img
    #srcPts = point_dict[name_list[4]]
    #dstPts = point_dict[name_list[3]]
    #p_mat_cR2R = calc_p_mat(numPts, srcPts, dstPts)
 
    #h_mat_cR2R = np.dot(np.linalg.inv(p_mat_cR2R[:,:-1]),-p_mat_cR2R[:,-1])
    #h_mat_cR2R = -1*np.append(h_mat_cR2R,1)
    #h_mat_cR2R.shape = (3,3)

    #print(h_mat_cR2R)
    
    tmpSz = (img_center.shape[1],img_center.shape[0])

    cLImg_warp = cv2.warpPerspective(img_center,h_mat_c2cL,tmpSz)
    #cLImg_warp = cv2.warpPerspective(img_centerL,h_mat,tmpSz)

    beta = (1-alpha)
    dstL = cv2.addWeighted(img_center,alpha,cLImg_warp,beta,0.0)

    tmpSz = (img_center.shape[1],img_center.shape[0])
    cRImg_warp = cv2.warpPerspective(img_centerR,h_mat_c2cR,tmpSz)
    dstR = cv2.addWeighted(img_center,alpha,cRImg_warp,beta,0.0)

    dst_total = cv2.addWeighted(dstL,alpha,dstR,beta,0.0)
    #cRImg_warp[0:cRImg_warp.shape[0],0:cRImg_warp.shape[1]] = cRImg_warp
    #cRImg_warp[0:img_centerR.shape[0],0:img_centerR.shape[1]] = img_centerR

    #lImg_warp = cv2.warpPerspective(img_right,h_mat_o*h_mat_c2cL*h_mat_cL2L,tmpSz)
    #rImg_warp = cv2.warpPerspective(img_right,h_mat_o*h_mat_c2cR*h_mat_cR2R,tmpSz)

    fig5 = plt.figure()
    endPlt = plt.imshow(dst_total)
    #fig4 = plt.figure()
    #endPltCL = plt.imshow((endImgCL*255).astype(np.uint8))
    plt.show()

    

if __name__=="__main__":
    main(numPts = int(10))
