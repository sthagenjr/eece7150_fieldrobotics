Readme for EECE7150 Project 1

Project 1.

Requirements

Anaconda w/ OpenCV 3.3.1 & Python 3.6
Install Packages: Numpy & Matplotlib

1) Run command: python proj1A_partA.py

When prompted, select 4 points from each image.

2) Run command: python proj1A_partB.py

When prompted, select 8 points from each image.

Use min least squares to reduce overdetermined H matrix

3) Run command: python proj1A_partC.py

When prompted, select 8 points from each image.

The addweight blending link, is a simple function that adds 2 images together to "blend" them with respective weights alpha & beta. The cmu link (Burt & Adelson) discusses a gaussian blend which uses a method of combining laplacian pyramids and data from an opitmal region of a gaussian pyramid to determine weights


4) Install jupyter notebook

run jupyter

open and run all pro1B_notebook.py.ipynb