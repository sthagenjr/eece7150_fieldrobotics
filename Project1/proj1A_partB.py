import cv2
import sys
import numpy as np
import matplotlib.pyplot as plt

DEBUG = False

#Function to calculate Project Matrix P coupled with t
def calc_p_mat(numPts, srcPts, dstPts):

    # Construct Empty List
    p_list = [] # Would have used np.empty but it was constructing with additional index...

    # Loop through selected Points
    for pt1, pt2 in zip(srcPts, dstPts):
        
        x = int(pt1[0])
        y = int(pt1[1])
        x_p = int(pt2[0])
        y_p = int(pt2[1])

        # From Lecture 2, hw 2.pdf Calculating homography
        tempArray = np.array([[x,y,1,0,0,0,-x*x_p,-y*x_p,-x_p],[0,0,0,x,y,1,-x*y_p,-y*y_p,-y_p]])
        tempArray.shape = (2,9)
        p_list.append(tempArray)

        if DEBUG == True:
            print('\n\n')
            print(p_list)

    p_mat = np.array(p_list)
    p_mat.shape = (2*numPts,9)

    return p_mat

#Main Function for AFR Project 1 Part A
def main(numPts):

    # Load Image 1 and Select Points
    img1 = cv2.imread('images/image1.jpg')

    fig1 = plt.figure()
    img1Plt = plt.imshow(img1)
    img1Pts = plt.ginput(n=numPts,timeout=90,show_clicks=True)

    print(img1Pts)

    # Clear Figure
    plt.clf()

    # Load Image 2 and Select Points
    img2 = cv2.imread('images/image2.jpg')

    fig2 = plt.figure()
    img2Plt = plt.imshow(img2)
    img2Pts = plt.ginput(n=numPts,timeout=90,show_clicks=True)

    print(img2Pts)

    # Calculate Project Matrix
    p_mat = calc_p_mat(numPts, img2Pts, img1Pts)

    print(p_mat)

    # Calculate H Matrix
    # For 2N Points N > 4, used least squares min. approx. 
    # h_mat = np.dot(np.linalg.inv(p_mat[:,:-1]),-p_mat[:,-1])
    h_mat,res,rank,s = np.linalg.lstsq(p_mat[:,:-1],-p_mat[:,-1],rcond=-1)
    h_mat = -1*np.append(h_mat,1)
    h_mat.shape = (3,3)

    print(h_mat)

    # Create Copy of Img1
    endImg = img1.copy()
    tmpSz = (endImg.shape[1],endImg.shape[0])

    if DEBUG:
        print("\n")
        print(tmpSz)
        print("\n")
        print(type(img2))

    # Warp Perspective was giving me a black image
    # Used this to make sure h_matrix was not the problem
    if DEBUG:
        
        srcPts = []
        dstPts = []
        
        for ptSrc,ptDst in zip(img2Pts,img1Pts):
            srcPts.append(ptSrc)
            dstPts.append(ptDst)
            srcArr = np.array(srcPts)
            dstArr = np.array(dstPts)
        
        h_mat2, status = cv2.findHomography(srcArr, dstArr)
        print('\n\n')
        print(h_mat2)
        
    # Warp Img2 and then Display it on Img1
    warpImg = cv2.warpPerspective(img2,h_mat,tmpSz)

    if DEBUG:
        plt.clf()
        tmpfig = plt.figure()
        warpPlt = plt.imshow(warpImg)
        plt.show()

        
    warpImg_mask = np.any(warpImg != [0,0,0], axis=-1)

    if DEBUG:
        print(warpImg_mask)

    endImg[warpImg_mask] = warpImg[warpImg_mask]

    plt.clf()
    fig3 = plt.figure()
    endPlt = plt.imshow(endImg)
    plt.show()
     
if __name__=="__main__":
    main(numPts = int(8))
