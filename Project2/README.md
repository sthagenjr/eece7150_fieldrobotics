Project 2 for EECE7150
The Underwater Dataset

To run, simply launch the jupyter notebook.
Open proj2_notebook.py.ipynb and run all.

There are flags that allow for toggling between data sets, debug flags, and flags for visualizing the matches between image pairs.

Output of the notebook is a g2o file that can be run through the gtsam (version 4.0.3) example Pose2SlamExample_g2o.py 